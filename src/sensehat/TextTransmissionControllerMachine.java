package sensehat;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

import mqtt.MQTTclient;
import runtime.IStateMachine;
import runtime.Scheduler;
import sensehat.Freepool;

public class TextTransmissionControllerMachine implements IStateMachine {

	private enum STATES {
		IDLE, DISPLAY, WAIT_FOR_MQTT
	}

	protected STATES state = STATES.IDLE;
	private Freepool freepool;

	// private final Semaphore semaphore = new Semaphore(1, true);

	public TextTransmissionControllerMachine(Freepool freepool) {
		this.freepool = freepool;
	}

	private String filterInputEvent(String event) {
		return event.substring(event.indexOf(':') + 1, event.length());
	}

	public void configureMQTTandTopic(MQTTclient mqttClient, String topic) {
		freepool.configureMQTTandTopic(mqttClient, topic);
	}

	// public Semaphore getSemaphore() {
	// return semaphore;
	// }

	public int fire(String event, Scheduler scheduler) {
		if (state == STATES.IDLE) {
			if (event.startsWith("MQTTMsg:freepool")) {
				freepool.receiveFreepool();
				state = STATES.IDLE;
				return EXECUTE_TRANSITION;
			} else if (event.startsWith("INPUT:")) {
				// start writing
				freepool.sendData(filterInputEvent(event));
				state = STATES.WAIT_FOR_MQTT;
				return EXECUTE_TRANSITION;
			}
		}
		if (state == STATES.WAIT_FOR_MQTT) {
			if (event.equals("MQTTReady")) {
				// start writing
				state = STATES.IDLE;
				return EXECUTE_TRANSITION;
			} else if (event.startsWith("MQTTMsg:freepool")) {
				freepool.receiveFreepool();
				state = STATES.IDLE;
				return EXECUTE_TRANSITION;
			}
		}
		return DISCARD_EVENT;
	}

	public static void main(String[] args) {
		String broker = "tcp://broker.hivemq.com:1883"; // args[0];
		String topic = "SenseHatText"; // args[1];
		String address = "SenseHatPCTTM4160";

		Semaphore semaphore = new Semaphore(5);

		//Semaphore textSemaphore = new Semaphore(1);
		Freepool freepool = new Freepool(5, semaphore);

		TextTransmissionControllerMachine stm = new TextTransmissionControllerMachine(freepool);
		Scheduler s = new Scheduler(stm);

		TextScanner scanner = new TextScanner(s, semaphore);

		MQTTclient mqttClient = new MQTTclient(broker, address, false, s);
		stm.configureMQTTandTopic(mqttClient, topic);

		mqttClient.subscribeToTopic(topic);

		scanner.start();
		s.start();
	}
}
