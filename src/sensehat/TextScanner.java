package sensehat;

import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import runtime.Scheduler;

public class TextScanner extends Thread {
    private Scheduler s;
    private Semaphore AllowTextInput;

    public TextScanner(Scheduler s, Semaphore semaphore) {
        this.s = s;
        AllowTextInput = semaphore;
    }

    public void run() {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while (true) {
            try {
                AllowTextInput.acquire();
                i++;
                // s.addToQueueLast("INPUT:" + sc.nextLine());
                s.addToQueueLast("INPUT:M" + String.valueOf(i));
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
