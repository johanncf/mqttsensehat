package sensehat;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

import mqtt.MQTTclient;

public class Freepool extends Thread {

    private LinkedList<String> freepoolsQueue;
    private LinkedList<String> bufferQueue;

    private MQTTclient mqttClient;
    private String topic = "";

    Semaphore allowNewMessages;

    public Freepool(int numberOfFreepools) {
        freepoolsQueue = new LinkedList<String>();
        bufferQueue = new LinkedList<String>();
        for (int i = 0; i < numberOfFreepools; i++) {
            freepoolsQueue.add("*");
        }
        allowNewMessages = new Semaphore(1);
    }

    public Freepool(int numberOfFreepools, Semaphore semaphore) {
        freepoolsQueue = new LinkedList<String>();
        bufferQueue = new LinkedList<String>();
        for (int i = 0; i < numberOfFreepools; i++) {
            freepoolsQueue.add("*");
        }
        allowNewMessages = semaphore;
    }

    public void configureMQTTandTopic(MQTTclient mqttClient, String topic) {
        this.mqttClient = mqttClient;
        this.topic = topic;
        mqttClient.subscribeToTopic("freepool");
    }

    public void sendData(String msg) {
        if (freepoolsQueue.isEmpty()) {
            bufferQueue.addLast(msg);
        } else {
            freepoolsQueue.pop();
            // send token and msg to the socket
            mqttClient.sendMessage("freepool", "*");
            mqttClient.sendMessage(topic, msg);
            //if (allowNewMessages.availablePermits() == 0) {
            //allowNewMessages.release();
            //}
        }   
    }

    public void sendFreepool() {
        freepoolsQueue.pop();
        // send token to the socket
        mqttClient.sendMessage("freepool", "*");
    }

    public void receiveFreepool() {
        freepoolsQueue.add("*");
        if (allowNewMessages.availablePermits() == 0) {
            allowNewMessages.release();
        }

        if (!bufferQueue.isEmpty()) {
            String msg = bufferQueue.pop();
            mqttClient.sendMessage(topic, msg);
        }
    }
}
