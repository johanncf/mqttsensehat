package mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import runtime.Scheduler;

public class MQTTclient implements MqttCallback {
	
	private Scheduler scheduler;
	private MqttClient client;
	private boolean confirmArrival;
	
	public MQTTclient(String broker, String myAddress, boolean conf, Scheduler s) {
		scheduler = s;
		confirmArrival = conf;
		MemoryPersistence pers = new MemoryPersistence();
		try {
			client = new MqttClient(broker,myAddress,pers);
			MqttConnectOptions opts = new MqttConnectOptions();
			opts.setCleanSession(true);
			client.connect(opts);
			client.setCallback(this);
			scheduler.addToQueueLast("MQTTReady");
		}
		catch (MqttException e) {
			System.err.println("MQTT Exception: " + e);
			scheduler.addToQueueLast("MQTTError");
		}
	}

	
	public void connectionLost(Throwable e) {
		scheduler.addToQueueLast("MQTTError:ConnectionLost");
	}
	
	public void deliveryComplete(IMqttDeliveryToken token) {
		scheduler.addToQueueLast("MQTTReady");
	}
	
	public void messageArrived(String topic, MqttMessage mess) {
		try {
			if (confirmArrival) {
				client.messageArrivedComplete(mess.getId(), mess.getQos());
			}
			scheduler.addToQueueLast("MQTTMsg:" + topic + ";" + mess.toString());
		} catch (MqttException e){
			System.err.println("MQTT Exception: " + e);
		}
	}

	public void sendMessage(String topic, String message) {
		MqttMessage mess = new MqttMessage(message.getBytes());
		try {
			mess.setQos(2);
			client.publish(topic, mess);
		} catch(MqttException e){
			System.err.println("MQTT Exception: " + e);
		}
	}
	
	public void subscribeToTopic(String topic) {
		try {
			client.subscribe(topic, 2);
		} catch (MqttException e){
			System.err.println("MQTT Exception: " + e);
		}
	}
}
